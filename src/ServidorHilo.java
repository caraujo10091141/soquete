// paquete para el flujo de datos
import java.io.*;
import java.net.*;
import javax.swing.JTextArea;

public class ServidorHilo extends Thread {
     // declaración de instancias globales
      JTextArea display;
      Socket conexion;
      ServerSocket server;
      DataOutputStream output;
      DataInputStream input;
     
     double s1,s2,suma;
     int clienteNum;
    
    public ServidorHilo(JTextArea d, ServerSocket s, Socket c,int n)
    {
       display=d;
       conexion=c;
       server=s;
       clienteNum=n;


        System.out.println("se ha entrado al constructor del Hilo cliente: "+ n);
        
    }
    public void run(){
        try {
           display.append("Se ha aceptado una conexión para el cliente: "+ clienteNum+"\n");
           display.append("\nRecibiendo sumandos...\n");  
           //Creación de las intancias para la transferencia de información
	   input = new DataInputStream(conexion.getInputStream());
           output = new DataOutputStream(conexion.getOutputStream());
	   
           display.append( "\nRecibiendo el primer sumando\n");
           s1= input.readDouble();
		
	   display.append( "\nRecibiendo el segundo sumando");
           s2 =input.readDouble();
                
           display.append("\nEnviando Resultado..\n");  
           suma= s1+s2;
           output.writeDouble(suma);	   
           display.append( "\n Transmisión terminada.\n Se cierra el socket del cliente: "+ clienteNum+"\n");
           conexion.close(); 		



       	conexion.close();
         }
      catch(IOException e){
                  e.printStackTrace();
                }

    }
}


