import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
// paquete que contiene las clases de sockets
import java.net.*;
// paquete que contiene las clases para el manejo de flujo de datos
import java.io.*;

public class Cliente extends JFrame implements ActionListener{

    Container c;
    JTextField op1,op2, ip;
    JLabel l1,l2,l3,l4;
    JPanel pN,pC,pS,pX;
    JPanel p1,p2,p3,p4;
    JButton boton;
    JTextArea display;
    
    public Cliente()
    {
        setTitle("Cliente de socket");
        setLayout(new GridLayout(4,1));
        c= getContentPane();
        c.setLayout(new FlowLayout());
        c.setBackground(Color.blue);
        // creación de las intancias y agregación a los paneles 
        // de las componentes GUI
        
        l1=new JLabel("Introuce por favor los valores para:");
        pN = new JPanel();
        pN.add(l1);
        
        l2=new JLabel("Primer sumando: ");
        op1=new JTextField(5);
        l3=new JLabel("Segundo sumando: ");
        op2=new JTextField(5);
        l4=new JLabel("IP servidor: ");
        ip=new JTextField(15);
        
        boton=new JButton("Servidor calcula la suma");
        
        pC = new JPanel();
        pC.add(l2);
        pC.add(op1);
        pC.add(l3);
        pC.add(op2);
        
        
        pX= new JPanel();
        pX.add(l4);
        pX.add(ip);
        pX.add(boton);
        
        
        display = new JTextArea(10,25);
        
        
        pS = new JPanel();
        pS.add(display);
        
        
        c.add(pN);
        c.add(pC);
        c.add(pX);
        c.add(pS);
          pack();
        this.setLocationRelativeTo(null);
        setVisible(true);
        setSize(450,350);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);
        
        
    }         

 @Override
    public void actionPerformed(ActionEvent e) {
       double s1,s2;    
    	s1=Double.parseDouble(op1.getText());
    	s2=Double.parseDouble(op2.getText());
    	System.out.println("Haciendo petición");
    	peticionServidor(s1,s2);
    }
    
    private void peticionServidor(double s1, double s2)
    {
        // declaración de un objecto para el socket cliente
        Socket client;
         // declaración de los objetos para el flujo de datos
         DataInputStream input;
         DataOutputStream output;
         double suma;    
         String Suma;
         try {
                // creación de la instancia del socket
                    client = new Socket(ip.getText(),6000);
		    display.setText("Socket Creado....\n");
                // creación de las instancias para el flujo de datos
                    input = new DataInputStream(client.getInputStream());
                    output = new DataOutputStream(client.getOutputStream());
	           display.append("Enviando primer sumando\n");
                  output.writeDouble(s1);
		     display.append("Enviando segundo sumando\n");
                  output.writeDouble(s2);
		     display.append ("El servidor dice....\n\n");
                  suma=input.readDouble();
                  Suma= String.valueOf (suma);
	           display.append("El  resultado es: "+ Suma+"\n\n");
                 display.append("Cerrando cliente\n\n");
                 client.close();
              }

                catch(IOException e){
                 e.printStackTrace();
                }
     }
    
    public static void main(String args[])
    {
        new Cliente();
        
    }
}


